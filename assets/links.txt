References:
https://github.com/mission-peace/interview/blob/master/src/com/interview/graph/CycleInDirectedGraph.java
https://trykv.medium.com/algorithms-on-graphs-directed-graphs-and-cycle-detection-3982dfbd11f5
https://github.com/eugenp/tutorials/blob/master/algorithms-miscellaneous-3/src/main/java/com/baeldung/algorithms/graphcycledetection/domain/Graph.java
https://www.geeksforgeeks.org/detect-cycle-in-a-graph/
https://www.geeksforgeeks.org/detect-cycle-direct-graph-using-colors/
https://www.geeksforgeeks.org/depth-first-search-or-dfs-for-a-graph/
https://stackoverflow.com/questions/261573/best-algorithm-for-detecting-cycles-in-a-directed-graph/261686#261686

https://www.baeldung.com/cs/detecting-cycles-in-directed-graph
https://groups.csail.mit.edu/tds/papers/Awerbuch/Info-Process-Letters85.pdf
https://github.com/aakash1104/Graph-Algorithms
https://github.com/sowmen/FXGraphAlgorithmSimulator
https://github.com/jagonmoy/Graph-Theory
https://gist.github.com/chrisco/ae1ba58b9df4f92e1db6c3adf39b71b0
https://github.com/christykmathew/Depth-First-and-Breadth-First-Search/blob/master/DFS%20and%20BFS.ipynb
https://github.com/SleekPanther/breadth-first-search-depth-first-search-graphs/blob/master/Graph.java
https://gist.github.com/tanaykumarbera/3c353990a27cb18ac53a
https://gist.github.com/obstschale/2973870
https://github.com/rahul1947/SP08-Depth-First-Search/blob/master/DFS.java
https://github.com/jcoreio/find-cycle/blob/master/directed.js
https://github.com/tmont/tarjan-graph/blob/master/index.js
https://github.com/carsonluuu/mycodebook/blob/master/bfs/detect-cycle-in-a-directed-graph.md

others:
https://trykv.medium.com/algorithms-on-graphs-directed-graphs-and-cycle-detection-3982dfbd11f5
https://scholar.google.com.br/scholar?hl=pt-BR&as_sdt=0%2C5&q=depth+first+search+algorithm&oq=depth+first+search
https://github.com/shady831213/algorithms
https://github.com/Erdos-Graph-Framework/Erdos
https://github.com/chen0040/js-graph-algorithms

First cycles example:
https://trello-attachments.s3.amazonaws.com/6078bfb60b16ad4e41323e56/128x253/edd1e3e7c0bdc87b4f61db642cd5d489/grafo-ex-3.png

DFS vs BFS Gif:
https://raw.githubusercontent.com/kdn251/interviews/master/images/dfsbfs.gif


https://www.geeksforgeeks.org/articulation-points-or-cut-vertices-in-a-graph/
https://www.geeksforgeeks.org/breadth-first-search-or-bfs-for-a-graph/
https://www.geeksforgeeks.org/topological-sorting/
https://www.geeksforgeeks.org/kruskals-minimum-spanning-tree-algorithm-greedy-algo-2/
https://www.geeksforgeeks.org/detect-negative-cycle-graph-bellman-ford/
https://www.geeksforgeeks.org/euler-circuit-directed-graph/



Important Links
https://github.com/mission-peace/interview/blob/master/src/com/interview/graph/AllCyclesInDirectedGraphJohnson.java
https://github.com/mission-peace/interview/blob/94be5deb0c0df30ade2a569cf3056b7cc1e012f4/src/com/interview/graph/AllCyclesInDirectedGraphTarjan.java#L25
https://github.com/mission-peace/interview/blob/master/src/com/interview/graph/CycleInDirectedGraph.java
