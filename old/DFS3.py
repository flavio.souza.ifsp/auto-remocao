# Ref: https://trykv.medium.com/algorithms-on-graphs-directed-graphs-and-cycle-detection-3982dfbd11f5
# Ref2: https://github.com/eugenp/tutorials/blob/master/algorithms-miscellaneous-3/src/main/java/com/baeldung/algorithms/graphcycledetection/domain/Graph.java


def cyclicGraphDetection(adj):
    visited = ['', 'N', '', '', 'N', '', '', 'N', 'N', '', '', '', 'N']

    for vertex in adj.keys():
        if visited[vertex] == 'N':
            stack = []
            stack.append(vertex)
            visited[vertex] = 'I'
            dfsCycleDetect(adj, stack, visited)


def dfsCycleDetect(adj, stack, visited):
    if peek(list(stack)) in adj:
        for vertex in adj[peek(list(stack))]:
            if visited[vertex] == 'I':
                print('Founded')
                printCycle(list(stack), vertex)
            elif visited[vertex] == 'N':
                stack.append(vertex)
                visited[vertex] = 'I'
                dfsCycleDetect(adj, stack, visited)
    
    print('peek', peek(list(stack)))

    visited[peek(list(stack))] = 'D'
    stack.pop()


def printCycle(stack, vertex):
    stack2 = []
    stack2.append(peek(stack))
    stack.pop()

    while peek(stack2) != vertex:
        stack2.append(peek(stack))
        stack.pop()
    
    while len(stack2) != 0:
        print(peek(stack2))
        stack.append(peek(stack2))
        stack2.pop()


def peek(stack):
    return stack[-1]


if __name__ == "__main__":
    matrix = {1: [11], 4: [1], 7:[8, 11], 8: [1, 4], 11: [8]}
    cyclicGraphDetection(matrix)
