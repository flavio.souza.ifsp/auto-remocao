# Ref:
# https://stackoverflow.com/questions/261573/best-algorithm-for-detecting-cycles-in-a-directed-graph/261686#261686

import collections

class Graph(object):
    def __init__(self, edges):
        self.edges = edges
        self.adj = Graph._build_adjacency_list(edges)

    @staticmethod
    def _build_adjacency_list(edges):
        adj = collections.defaultdict(list)
        for edge in edges:
            adj[edge[0]].append(edge[1])
        return adj


def findCycles(G):
    discovered = set()
    finished = set()

    for u in list(G.adj):
        if u not in discovered and u not in finished:
            discovered, finished = dfsVisit(G, u, discovered, finished)


def dfsVisit(G, u, discovered, finished):
    discovered.add(u)

    for v in G.adj[peek(list(discovered))]:
        # Detect cycles
        if v in discovered:
            print(f"Cycle detected: found a back edge from {u} to {v}.")
            print('Discovered way:')
            printCycle(list(discovered), v)
            break

        # Recurse into DFS tree
        if v not in finished:
            dfsVisit(G, v, discovered, finished)

    discovered.remove(u)
    finished.add(u)

    return discovered, finished

def printCycle(stack, v):
    stack2 = []
    stack2.append(peek(stack))
    stack.pop()

    while peek(stack2) != v:
        stack2.append(peek(stack))
        stack.pop()
    
    while len(stack2) != 0:
        print(peek(stack2))
        stack.append(peek(stack2))
        stack2.pop()


def peek(stack):
    return stack[-1]


if __name__ == "__main__":
    G = Graph([
        # ('0', '5'),
        # ('0', '6'),
        # ('2', '0'),
        # ('2', '3'),
        # ('3', '6'),
        # ('3', '10'),
        ('1', '11'),
        ('4', '1'),
        # ('5', '2'),
        # ('5', '10'),
        # ('6', '2'),
        ('7', '8'),
        ('7', '11'),
        ('8', '1'),
        ('8', '4'),
        # ('10', '3'),
        ('11', '8')
    ])

    findCycles(G)
