# https://www.geeksforgeeks.org/union-find/

import sys
from CSVReader import CSVReader
from Graph import Graph as GraphClass


def main(spreadsheet):
    graph = GraphClass(True)

    csv = CSVReader()
    edges, biggest_vertex, labels = csv.read(graph, spreadsheet)

    union_find(graph)


def find_parent(graph, parent, i):
    if parent[i] == -1:
        return i
    if parent[i]!= -1:
            return graph.find_parent(parent,parent[i])


def union(parent, x, y):
    parent[x] = y


def union_find(graph):
    parent = [-1] * len(graph.all_vertex.values())

    for i in graph.all_vertex.values():
        for j in graph.all_vertex.get(i):
            x = find_parent(graph, parent, i.id)
            y = find_parent(graph, parent, j.id)
            if x == y:
                print(i)
                print(j)
                print(x)
                print(y)
                print(parent)
                return True
            union(parent, x, y)


if __name__ == '__main__':
    main(sys.argv[1])
