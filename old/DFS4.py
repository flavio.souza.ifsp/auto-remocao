from collections import defaultdict
  
class Graph:
    def __init__(self, v):
        self.v = v
        self.graph = defaultdict(list)
  
    def addEdge(self, u, v):
        self.graph[u].append(v)
  
    def DFSUtil(self, u, color):
        color[u] = "GRAY"
  
        for v in self.graph[u]:
  
            if color[v] == "GRAY":
                return True
  
            if color[v] == "WHITE" and self.DFSUtil(v, color) == True:
                return True
  
        color[u] = "BLACK"
        return False
  
    def isCyclic(self):
        color = ["WHITE"] * self.v
  
        for i in range(self.v):
            if color[i] == "WHITE":
                if self.DFSUtil(i, color) == True:
                    print("OK")                    
                    print(color)                    
                    return True
        return False
  
g = Graph(12)
g.addEdge(1, 11)
g.addEdge(4, 1)
g.addEdge(7, 8)
g.addEdge(7, 11)
g.addEdge(8, 1)
g.addEdge(8, 4)
g.addEdge(11, 8)

if g.isCyclic() == True:
    print("Graph contains cycle")
else :
    print("Graph doesn't contain cycle")