# Auto-Remoção Project

>Find cycles in direted graphs.

### Tools and Technologies

- Python3+
- IGraph
- Pandas

### Installation
To install and run the project you want to execute the following commands:
```bash
#Clone the project
$ git clone https://gitlab.com/flavio.souza.ifsp/auto-remocao.git

$ cd auto-remocao

#Install Projecte Dependecys
$ pip install -r requirements.txt 

#Run the main code
$ python3 main.py

#Run tests
$ python3 DFS{version}.py
```
