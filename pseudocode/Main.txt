Entrada Main: 
main(algorithm, spreadsheet)
algorithm ← 'TARJAN'

Processamento:
my_graph ← graph_class
csv ← read_csv()
all_cycles ← {}

vertices, labels ← csv.read(my_graph, spreadsheet)

if algorithm = 'DFS'
    all_cycles ← dfs(my_graph, vertices, labels)
elif algorithm = 'BFS'
    all_cycles ← bfs(my_graph, vertices, labels)
elif algorithm = 'TARJAN'
    all_cycles ← tarjan(my_graph, vertices, labels)
else
    print('Invalid Algorithm, try again.')
    return

if len(all_cycles) > 0
    for cycle in all_cycles
        graph ← create_graph(cycle['cycle'], cycle['biggest_vertex'], cycle['labels'])
        plot(graph, layout='layout_kamada_kawai')
else
    print('This graph has no cycle, try again.')
    return

create_graph(edges, quantity_vertices, labels)
    graph ← graph()
    graph.add_vertices(quantity_vertices + 1)
    graph.add_edges(edges)
    graph['label'] ← labels

    return graph

Retorno
Mostrar todos os ciclos encontrados

Listar as classes (funções) específicas de detecção de ciclos 
Class main:  
    main(graph)
        - Chamar leitura dos dados inputados
        - Chamar processamento dos dados de entrada para validação dos ciclos existentes
        - Chamar função de plotagem do ciclos encontrados
    create_graph(edges, quantity_vertices, labels)
        - Preparando a estrutura do grafo para plotagem

Saída:
Caso exista um ou mais plot dos ciclos encontrados no grafo