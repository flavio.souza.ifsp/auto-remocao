from Graph import Graph as GraphClass
from igraph import Graph, plot
import sys
from CSVReader import CSVReader
from BFS import bfs
from DFS import dfs
from Tarjan import tarjan
from Johnson import johnson


def main(algorithm, spreadsheet):
    my_graph = GraphClass(True)
    csv = CSVReader()
    cyclesObj = {}

    vertices, labels = csv.read(my_graph, spreadsheet)

    if algorithm == 'DFS':
        cyclesObj = dfs(my_graph, vertices, labels)
    elif algorithm == 'BFS':
        cyclesObj = bfs(my_graph, vertices, labels)
    elif algorithm == 'TARJAN':
        cyclesObj = tarjan(my_graph, vertices, labels)
    elif algorithm == 'JOHNSON':
        cyclesObj = johnson(my_graph)

    if len(cyclesObj) > 0:
        for cycle in cyclesObj:

            graph = create_graph(cycle['cycles'], cycle['biggest_vertex'], cycle['labels'])

            default_plot(graph)


def create_graph(edges, vertices, labels):
    g = Graph(directed=True)
    g.add_edges(edges)
    g.vs["label"] = labels

    return g


def default_plot(graph):
    plot(graph, layout=graph.layout("kk"))


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
