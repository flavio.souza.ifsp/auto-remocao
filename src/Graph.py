from Vertex import Vertex
from Edge import Edge


class Graph:
    def __init__(self, is_directed):
        self.all_edges = []
        self.all_vertex = {}
        self.is_directed = is_directed

    def add_edge(self, id1, id2, weight=0):
        if id1 in self.all_vertex:
            vertex1 = self.all_vertex[id1]
        else:
            vertex1 = Vertex(id1)
            self.all_vertex[id1] = vertex1

        if id2 in self.all_vertex:
            vertex2 = self.all_vertex[id2]
        else:
            vertex2 = Vertex(id2)
            self.all_vertex[id2] = vertex2

        edge = Edge(vertex1, vertex2, self.is_directed, weight)
        self.all_edges.append(edge)
        vertex1.add_adjacent_vertex(edge, vertex2)
        
        if self.is_directed is not True:
            vertex2.add_adjacent_vertex(edge, vertex1)

    def cycle_set_to_igraph_format(self, detected_cycles):
        cycle_formatted = []

        for vertexes in self.all_edges:
            for vertex in detected_cycles:
                if vertexes.vertex1 == vertex and vertexes.vertex2 in detected_cycles:
                    source = vertexes.vertex1.id
                    destination = vertexes.vertex2.id

                    cycle_formatted.append(tuple((source, destination)))

        return cycle_formatted

    def cycle_deque_to_set(self, deque, vertices, labels):
        biggest_vertex = 0
        cycleSet = set()

        for item in deque:
            cycleSet.add(item)
            if item.id > biggest_vertex:
                biggest_vertex = item.id
        
        return {
            'labels': labels,
            'cycles': self.cycle_set_to_igraph_format(cycleSet),
            'biggest_vertex': vertices
        }

    def check_if_cycle_exists(self, cycle_to_check, existing_cycles):
        if len(existing_cycles) > 0:
            for cycles_object in existing_cycles:
                supportStack = cycles_object.get('cycles')

                if len(cycle_to_check) == len(cycles_object.get('cycles')):
                    for vertex in cycles_object.get('cycles'):
                        if vertex in cycle_to_check:
                            supportStack.pop(supportStack.index(vertex))

                    if len(supportStack) == 0:
                        print(cycle_to_check)
                        print(cycles_object.get('cycles'))
                        return True
        return False
