def bfs(graph, biggest_vertex, labels):
    final_result = []

    for vertex in graph.all_vertex.values():
        queue = []
        visited = [False] * (len(graph.all_vertex.values()) + 1)

        first_vertex = graph.all_vertex.get(vertex.id)
        queue.append(first_vertex.id)
        visited[first_vertex.id] = True
        detected_cycle = set()

        while queue:
            s = queue.pop(0)

            for vertex in graph.all_vertex.get(s).adjacent_vertices:
                idx = list(graph.all_vertex.keys())[list(graph.all_vertex.values()).index(vertex)]
                if visited[idx] == False:
                    queue.append(idx)
                    visited[idx] = True
                    detected_cycle.add(vertex)
        detected_cycle.add(first_vertex)

        formatted_cycle = graph.cycle_set_to_igraph_format(detected_cycle)
        cycle_exists = False # graph.check_if_cycle_exists(formatted_cycle, final_result)

        if not cycle_exists:
            final_result.append({
                'labels': labels,
                'cycles': formatted_cycle,
                'biggest_vertex': biggest_vertex
            })

    return final_result