# detect cycle in directed graph
# https://github.com/mission-peace/interview/blob/94be5deb0c0df30ade2a569cf3056b7cc1e012f4/src/com/interview/graph/AllCyclesInDirectedGraphJohnson.java

from Graph import Graph as GraphClass
from collections import deque
import sys

blockedSet = set()
blockedMap = {}
stack = deque()
allCycles = []
visitedTime = dict()
lowTime = dict()
onStack = set()
visited = set()
result = []
times = 0


def main():
    graph =  GraphClass(True)
    graph.add_edge(1, 2)
    graph.add_edge(1, 8)
    graph.add_edge(1, 5)
    graph.add_edge(2, 9)
    graph.add_edge(2, 7)
    graph.add_edge(2, 3)
    graph.add_edge(3, 1)
    graph.add_edge(3, 2)
    graph.add_edge(3, 6)
    graph.add_edge(3, 4)
    graph.add_edge(6, 4)
    graph.add_edge(4, 5)
    graph.add_edge(5, 2)
    graph.add_edge(8, 9)
    graph.add_edge(9, 8)

    allCycles = johnson(graph)

    print('allCycles', allCycles)

    for cycle in allCycles:
        for vertex in cycle:
            print(vertex.id + " ")
        print("")


def johnson(graph):
    global blockedSet
    global blockedMap
    global stack
    global allCycles
    global startIndex

    blockedSet = set()
    blockedMap = {}
    stack = deque()
    allCycles = []
    startIndex = 1

    while startIndex <= len(graph.all_vertex):
        subGraph = createSubGraph(startIndex, graph)
        sccs = scc(subGraph)
        print('sccs')
        print(sccs)
        maybeLeastVertex = leastIndexSCC(sccs, subGraph)
        if maybeLeastVertex:
            leastVertex = maybeLeastVertex
            blockedSet.clear()
            blockedMap.clear()
            findCyclesInSCG(graph, leastVertex, leastVertex)
            startIndex = leastVertex.id + 1
        else:
            break
    return allCycles


def leastIndexSCC(sccs, subGraph):
    minimum = 1000000000
    minVertex = None
    minScc = None

    for scc in sccs:
        if len(scc) == 1:
            continue
        for vertex in scc:
            if vertex < minimum:
                minimum = vertex
                minVertex = vertex
                minScc = scc

    if minVertex == None:
        return []

    graphScc = GraphClass(True)

    for edge in subGraph.all_edges:
        if edge.vertex1.id in minScc and edge.vertex2.id in minScc:
            graphScc.add_edge(edge.vertex1.id, edge.vertex2.id)

    if len(graphScc.all_vertex) == 0:
        return []
    for graph_scc_vertex in graphScc.all_vertex.values():
        if minVertex == graph_scc_vertex.id:
            return minVertex
    return []


def unblock(u):
    if u in blockedSet:
        blockedSet.remove(u)
    if u in blockedMap:
        print("blockedMap[u]")
        print(u)
        print(blockedMap[u])
        if blockedMap[u] in blockedSet:
            unblock(u)
        del blockedMap[u]


def findCyclesInSCG(graph, startVertex, currentVertex):
    foundCycle = False
    stack.append(currentVertex)
    blockedSet.add(currentVertex)

    current_vertex_edges = graph.all_vertex.get(currentVertex).edges
    current_vertex_edges.sort(key=lambda e: e.vertex2.id)

    print(currentVertex)

    for e in current_vertex_edges:
        neighbor = e.vertex2.id
        if neighbor == startVertex:
            cycle = []
            stack.append(startVertex)
            cycle.append(stack)
            cycle = reversed(cycle)
            stack.pop()
            allCycles.append(cycle)
            foundCycle = True
        elif neighbor not in blockedSet:
            gotCycle = findCyclesInSCG(graph, startVertex, neighbor)
            foundCycle = foundCycle or gotCycle

    if foundCycle:
        unblock(currentVertex)
    else:
        for e in current_vertex_edges:
            w = e.vertex2
            bSet = {}
            bSet = getBSet(w)
            bSet[len(bSet)] = currentVertex
            print('bSet')
            print(bSet)
    stack.pop()
    return foundCycle


def getBSet(v):
    global blockedMap
    blockedMap[v.id] = blockedMap.get(v.id, 0) + 1
    return blockedMap


def createSubGraph(startVertex, graph):
    subGraph = GraphClass(True)
    for edge in graph.all_edges:
        if edge.vertex1.id >= startVertex and edge.vertex2.id >= startVertex:
            subGraph.add_edge(edge.vertex1.id, edge.vertex2.id)
    return subGraph


def scc(graph):
    global times
    global visitedTime
    global lowTime
    global onStack
    global stack
    global visited
    global result

    times = 0
    visitedTime = dict()
    lowTime = dict()
    onStack = set()
    stack = deque()
    visited = set()
    result = []

    for vertex in graph.all_vertex.values():
        if vertex.id in visited:
            continue
        scc_util(vertex)

    return result


def scc_util(vertex):
    global times
    visited.add(vertex.id)
    visitedTime[vertex.id] = times
    lowTime[vertex.id] = times
    times += 1
    stack.appendleft(vertex.id)
    onStack.add(vertex.id)

    for child in vertex.adjacent_vertices:
        if child.id not in visited:
            scc_util(child)
            lowTime[vertex.id] = min(lowTime[vertex.id], lowTime[child.id])
        elif child.id in onStack:
            lowTime[vertex.id] = min(lowTime[vertex.id], visitedTime[child.id])

    if visitedTime[vertex.id] == lowTime[vertex.id]:
        strongly_donnected_componenet = []
        v = None

        while vertex.id != v:
            v = stack.popleft()
            onStack.remove(v)
            strongly_donnected_componenet.append(v)
        strongly_donnected_componenet.sort()
        result.append(strongly_donnected_componenet)


if __name__ == '__main__':
    main()
