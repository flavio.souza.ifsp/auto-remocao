# detect all cycles in directed graph with tarjan algorithm
# https://github.com/mission-peace/interview/blob/94be5deb0c0df30ade2a569cf3056b7cc1e012f4/src/com/interview/graph/AllCyclesInDirectedGraphTarjan.java#L25

from collections import deque

visited = set()
pointStack = deque()
markedStack = deque()
markedSet = set()
finalResult = []

def tarjan(graph, vertices, labels):
    for vertex in graph.all_vertex.values():
        find_all_simple_cycles(graph, vertex, vertex, vertices, labels)
        visited.add(vertex)
        while len(markedStack) > 0:
            to_remove = markedStack[0]
            markedStack.remove(to_remove)
            markedSet.remove(to_remove)

    return finalResult


def find_all_simple_cycles(graph, start, current, vertices, labels):
    hasCycle = False
    pointStack.append(current)
    markedSet.add(current)
    markedStack.appendleft(current)

    if current.adjacent_vertices:
        for adjacentVertex in current.adjacent_vertices:
            if adjacentVertex in visited:
                continue
            elif adjacentVertex == start:
                hasCycle = True
                pointStack.append(adjacentVertex)
                pointStack.popleft()

                finalResult.append(graph.cycle_deque_to_set(pointStack, vertices, labels))
            elif adjacentVertex not in markedSet:
                hasCycle = find_all_simple_cycles(graph, start, adjacentVertex, vertices, labels) or hasCycle

    if hasCycle:
        while markedStack[0] != current:
            to_remove = markedStack[0]
            markedStack.remove(to_remove)
            markedSet.remove(to_remove)

        to_remove_2 = markedStack[0]
        markedStack.remove(to_remove_2)
        markedSet.remove(to_remove_2)

    pointStack.popleft()
    return hasCycle
