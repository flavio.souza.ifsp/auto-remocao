# detect cycle in directed graph
# https://github.com/mission-peace/interview/blob/master/src/com/interview/graph/CycleInDirectedGraph.java

import sys
from Vertex import Vertex


def dfs(graph, biggest_vertex, labels):
    finalResult = []

    for vertex in graph.all_vertex.values():
        white = set()
        gray = set()
        black = set()

        for vertex in graph.all_vertex.values():
            white.add(vertex)

        white.add(vertex)

        while len(white) > 0:
            current = next(iter(white))
            if find_cycle(current, white, gray, black):
                finalResult.append({
                    'labels': labels,
                    'cycles': graph.cycle_set_to_igraph_format(gray),
                    'biggest_vertex': biggest_vertex
                })

    return finalResult


def find_cycle(current, white, gray, black):
    move_vertex(current, white, gray)
    for neighbor in current.adjacent_vertices:
        if neighbor in black:
            continue
        if neighbor in gray:
            return True
        if find_cycle(neighbor, white, gray, black):
            return True

    move_vertex(current, gray, black)
    return False


def move_vertex(vertex, source_set, destination_set):
    source_set.remove(vertex)
    destination_set.add(vertex)
