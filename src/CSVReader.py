import pandas as pd
import os


class CSVReader():
    def read(self, graph, spreadsheet='grafo-ex-3.xlsx'):
        df = pd.read_excel(os.path.dirname(__file__) + '/../assets/' + spreadsheet, engine='openpyxl')
        out = df.to_numpy().tolist()

        labels = []
        biggestVertex = 0

        for values in out:
            source = int(values[0])
            if source not in labels:
                labels.append(source)

            for target in values[1:]:
                if pd.isna(target):
                    continue

                target = int(target)

                if target > biggestVertex:
                    biggestVertex = target

                graph.add_edge(source, target)

        return int(biggestVertex + 1), labels